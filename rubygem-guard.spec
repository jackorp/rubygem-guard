# Generated from guard-2.14.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name guard

Name: rubygem-%{gem_name}
Version: 2.14.2
Release: 1%{?dist}
Summary: Guard keeps an eye on your file modifications
License: MIT
URL: http://guardgem.org
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/guard/guard.git && cd guard
# git checkout v2.14.2
# tar -czvf rubygem-guard-2.14.2-spec.tar.gz spec/
Source1: %{name}-%{version}-spec.tar.gz
# Fix rspec random test failure
# https://github.com/guard/guard/pull/894
Patch0: rubygem-guard-2.14.2-fix-rspec-random-test-failure.patch
# Fix rspec warning message
# https://github.com/guard/guard/pull/893
Patch1: rubygem-guard-2.14.2-fix-rspec-warning-message.patch
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby >= 1.9.3
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(formatador)
BuildRequires: rubygem(listen)
BuildRequires: rubygem(lumberjack)
BuildRequires: rubygem(nenv)
BuildRequires: rubygem(notiffany)
BuildRequires: rubygem(pry)
BuildRequires: rubygem(shellany)
BuildRequires: rubygem(thor)
BuildArch: noarch

%description
Guard is a command line tool to easily handle events on file system
modifications.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version} -b 1

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

pushd %{_builddir}/
%patch0 -p1
%patch1 -p1
popd

# Fix Shebang
sed -i -e 's|#!/usr/bin/env ruby|#!/usr/bin/ruby|' bin/%{gem_name} bin/_%{gem_name}-core

# Kill Shebang
sed -i -e '\|^#!|d' lib/guard/rake_task.rb

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -pa .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

# Run the test suite.
%check
pushd .%{gem_instdir}
ln -s %{_builddir}/spec spec

# We don't really care about code coverage.
sed -i "/[sS]imple[cC]ov/ s/^/#/" spec/spec_helper.rb

# RPM has some unexpected environment variables, ignore them.
sed -i '/GEM_SKIP/a \    allow(ENV).to receive(:[]).with("RPM_PACKAGE_NAME").and_call_original' spec/spec_helper.rb

# TODO: Fails with "stub me! (File.exist?("/usr/lib/gems/ruby/ffi-1.9.18/gem.build_complete"))",
# not entirely sure why
sed -i '/it "shows an info message" do/,/^      end$/ s/^/#/' spec/lib/guard/plugin_util_spec.rb

rspec -rspec_helper spec
popd


%files
%dir %{gem_instdir}
%{_bindir}/guard
%{_bindir}/_guard-core
%license %{gem_instdir}/LICENSE
%{gem_instdir}/bin
%{gem_instdir}/images
%{gem_libdir}
%{gem_instdir}/man
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md

%changelog
* Wed Dec 06 2017 Jaroslav Prokop <jar.prokop@volny.cz> - 2.14.2-1
- Initial package
